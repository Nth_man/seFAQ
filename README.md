# Some frequently asked questions (FAQ) about safety and efficiency in C, C++ and Rust


## Table of Contents

- [What is a sound static analyzer?](#what-is-a-sound-static-analyzer)
- [If I programmed in C or C++, would I need a sound static analyzer?](#if-i-programmed-in-c-or-c-would-i-need-a-sound-static-analyzer)
- [If I programmed in Rust, would I need a sound static analyzer](#if-i-programmed-in-rust-would-i-need-a-sound-static-analyzer) 


## What is a sound static analyzer?

You are maybe already familiar with static analyzers, such as SpotBugs (formerly FindBugs), but these popular analyzers tend to focus on bug patterns. Sound static analysis is a slightly different beast. It is far more interesting because the results are as reliable as mathematical proofs! The NIST report on [Dramatically Reducing Software Vulnerabilities](https://nvlpubs.nist.gov/nistpubs/ir/2016/NIST.IR.8151.pdf) emphasizes the need for sound analysis and explains the distinction neatly: "Heuristic analysis is faster than sound analysis, but lacks assurance that comes from a chain of logical reasoning".

Learning to develop and effectively apply sound static analyzers is much more difficult than just writing bug patterns. This is, however, a skill that is increasingly sought after. All the big silicon valley companies are growing their static analysis teams. The following is an example of the requirements for a Static Analysis Engineering job at Facebook: https://sws.cs.ut.ee/Thesis/FbJob  
        _-- https://sws.cs.ut.ee/Thesis/SoundAnalysis_ (University of Tartu)

___

[A sound static analyzer] is used to prove absence of runtime errors in:

- Airbus flight control software
- Docking software for the International Space Station

See [https://absint.com/astree](https://absint.com/astree)  
        _-- https://courses.cs.washington.edu/courses/cse403/15sp/lectures/L19.pdf_ (University of Washington)

___

Not all static analysis tools work the same, there are in fact a spectrum of tools that use a variety of techniques ranging from relatively simple syntactic analysis through very sophisticated abstract interpretation-like algorithms that reason about potential executions. Each approach has its strengths and weaknesses and often tools, like GrammaTech CodeSonar, use a combination of syntax analysis and abstract interpretation to achieve their goals. Sound static analyzers are a lesser known flavor of analyzers that aim to prove the absence of runtime errors in code. These tools use formal methods to prove that certain aspects of a program are true (e.g., “the program is memory safe”). 

The term “soundness” as it applies to static analysis means that if a bug exists in the code, a sound analysis is guaranteed to report it. The key property of these tools is that if they cannot rigorously prove that a defect is absent, they must report that potential defect as a warning.  
        _-- https://blogs.grammatech.com/how-sound-static-analysis-complements-heuristic-analysis_

___

Safety-critical embedded software has to satisfy stringent quality requirements. One such requirement, imposed by all contemporary safety standards, is that no critical run-time errors must occur. Runtime errors can be caused by undefined or unspecified behavior of the programming language; examples are buffer overflows or data races. They may cause erroneous or erratic behavior, induce system failures, and constitute security vulnerabilities. 

A sound static analyzer reports all such defects in the code, or proves their absence. Sound static program analysis is a verification technique recommended by ISO/FDIS 26262 for software unit verification and for the verification of software integration. 

In this article we propose an analysis methodology that has been implemented with the static analyzer Astrée. It supports quick turn-around times and gives highly precise whole-program results. We give an overview of the key concepts of Astrée that enable it to efficiently handle large-scale code, and describe a pre-analysis which transforms the source code to make it better amenable to static analysis. The experimental results confirm that sound static analysis can be successfully applied for integration verification of large-scale automotive software.  
        _-- https://www.sae.org/publications/technical-papers/content/2019-01-1246/_  
        _-- https://doi.org/10.4271/2019-01-1246_

___

In recent years, security concerns have become more and more relevant for safety-critical systems. Many cybersecurity vulnerabilities are caused by runtime errors, hence sound static runtime error analysis contributes to meeting both safety and security goals. In addition, for cybersecurity goals, often sophisticated data and control flow analyses are needed, e.g., to track the effects of corrupted values, or determine dependence on potentially corrupted inputs. A sound analysis can guarantee that neither control flow paths nor read or write accesses are missed, even in case of {data or function} pointer accesses. [...] Practical experience is reported on industrial avionic and automotive applications. [...] 

Examples are faulty pointer manipulations, numerical errors such as arithmetic overflows and division by zero, data races and synchronization errors in concurrent software. [...] Abstract interpretation supports formal soundness proofs, i.e., it can be proven that – from the class of errors under analysis – no error is missed. Sound static analyzers provide full control and data coverage and allow conclusions to be drawn that are valid for all program runs with all inputs.  
        _-- https://hal.science/hal-02479217/document_

___

[A sound static analyzer] does not solve all problems. It just provides a way to write code that has no runtime problems. Logical problems such as the code being expected to sort numbers in ascending order and instead sorting it in descending order are not caught by it.

That said, it has an advertising problem outside of the nuclear and aerospace sectors, since almost nobody outside of those sectors knows about it.  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1385878#post1385878`)_


## If I programmed in C or C++, would I need a sound static analyzer?

If a [sound static analyzer](#what_is_a_sound_static_analyzer) were deployed, we could make C code with zero memory safety issues and do it without the hack of compiler added runtime checks that Rust uses to claim memory safety. The aviation and nuclear power industries have been doing this for years. It is a shame that no one is willing to follow their lead and the wider community instead pursues new languages when what it actually needs is better tooling, which at present only exists for C (and C++ if you don't insist on having a formally verified compiler). Those new languages do not have such tooling and need it to reach parity with what is possible with C when using tools like sound static analyzers and formally verified compilers. :/  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1385835#post1385835)_

Additionally, some techniques are helpful in several cases. For example:

- [RAII](https://en.cppreference.com/w/cpp/language/raii), 
- using containers,
- (when creating an object) handling the ownership (the responsibility to clean up) over to the parent object.

A list that includes sound static analyzers (with others that are not sound) for C:  
https://github.com/analysis-tools-dev/static-analysis#c
        
A list that includes sound static analyzers (with others that are not sound) for C++:  
https://github.com/analysis-tools-dev/static-analysis#cpp

CVEs (Common Vulnerabilities and Exposures) in C++ programs:  
https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=C%2B%2B


## If I programmed in Rust, would I need a sound static analyzer?

Yes, the advantages of a [sound static analyzer](#what-is-a-sound-static-analyzer) are needed for reasons that are similar to [those in the C or C++ case](#if-i-programmed-in-c-or-c-would-i-need-a-sound-static-analyzer). A C++ borrow checker (or a Rust one) does not bring the aforementioned advantages.
    
CVEs (Common Vulnerabilities and Exposures) in Rust programs:  
https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=rust

[Static Analyzer Rudra Found over 200 Memory Safety Issues in Rust Crates](https://www.infoq.com/news/2021/11/rudra-rust-safety/).  
Developed at the Georgia Institute of Technology, Rudra is a static analyzer able to report potential memory safety bugs in Rust programs. Rudra has been used to scan the entire Rust package registry and identified 264 new memory safety bugs.

___

[We see someone talking] about the false necessity of rewriting C code when the issue is provably a matter of tooling and developer discipline. The reality is that Turing completeness makes all languages equivalent, so pursuing rewrites for whatever syntactic sugar a newer Turing-complete language gives is pointless. No matter what language you use, you need tools that can prove the absence of runtime issues as a prerequisite for writing reliable software. Without those tools and the developer discipline to use them, you will always have these issues. Given that these tools seem to only exist for C/C++, going to a different language is likely detrimental versus adopting them and having the discipline to make code pass them.  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1385857#post1385857)_

___

Unless you are doing bindings to assembly or another language, you can analyze whatever is the C/C++ equivalent of the “unsafe rust” using Astree, so in code where Astree is being used, you would not have those issues in the first place. Rust needs these tools too.

Anyway, we would be better off deploying sound static analysis tools for C/C++ than we would be rewriting things in Rust. Rewriting things in Rust means that we need to reinvent these tools for Rust and that is a process that takes decades, by which time people will surely be saying to switch to yet another language and we will be back to where we are now in our ability to produce software without runtime issues. :/  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1385867#post1385867)_

___

There are a few free sound static analysis tools, but none are as mature as Astree as far as I can tell. NASA IKOS for example does not support concurrency. Anyway, rustc also gives weaker guarantees than Astree, so it not a replacement. Rustc is also not formally verified, so it can miscompile code. It is not a replacement for CompCert either. Rust needs both a sound static analyzer and a formally verified compiler.

I would really like to see more effort go into making open source replacements for Astree and CompCert, but they are so rarely mentioned that the people who would write open source replacements if they knew about them simply do not know, which means they do not get started. I hope my mention of them helps to get the word out to those people, since I have my hands full with my existing work.  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1385870#post1385870)_

___

> You're touting ASTree and the like for C and how superior that is, I assume you have access? 

You can get a 30-day free trial to Astree. Feel free to use that to do the exercise that you envision.

> Without looking at the drivers current code with all those fixes, how long would it take for you to use that tooling and get all the already known issues fixed, and how likely would it match the existing C driver do you think?

Much less time than a rewrite. You would still need equivalent tools for Rust to get full safety.

Astree gained C++ support around 2018. It only supported C until that point. As far as I can tell, it did not support dynamic memory allocation until around 2016. It existed as early as 2001, and it is advertised that it was used to prove the absence of runtime issues in the Airbus A340 fly-by-wire system in November 2003.

It does not solve all problems. It just provides a way to write C/C++ code that has no runtime problems. Logical problems such as the code being expected to sort numbers in ascending order and instead sorting it in descending order are not caught by it.

That said, it has an advertising problem outside of the nuclear and aerospace sectors, since almost nobody outside of those sectors knows about it.

I should mention that Astree is not the only option here. The Polyspace Code Prover is another option. However, Astree is the only one that received a glowing review from NIST: https://nvlpubs.nist.gov/nistpubs/ir/2020/NIST.IR.8304.pdf

There actually was one more that did, but Frama-C's Eva does not support recursion or branches, so I do not consider it to be a real option. It is open source, but it suffers from a problem that it needs documentation for how to navigate its documentation, so it is not an easy to approach project. :/

> I know you've raised some negatives against rust, but you feel that those would be more difficult to resolve upstream (if necessary) than what you propose for addressing the concerns present with C?

Writing a sound static analyzer for Rust and a formally verified Rust compiler is far harder than simply using the existing tools for C. It would likely take more than a decade and by that time, people will probably be saying to rewrite everything in some other language, like Zig. :/  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1385878#post1385878)_
        
___

Rust certainly has its merits, but the idea that rewriting existing code in Rust automatically produces a better result is demonstrably false: https://arxiv.org/pdf/2008.06537.pdf

Those guys are huge fans of the idea that rewriting code in Rust produces better results, but contrary to their expectations, they found that the current state of rewrites is no better than the code it is intended to replace.  
        _-- [Richard Yao](https://www.phoronix.com/forums/forum/software/general-linux-open-source/1385644-rust-null-block-driver-published-to-begin-experimenting-with-rust-for-linux-storage?p=1386053#post1386053)_
